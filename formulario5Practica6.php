<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    if ($mal) {

    ?>
        <form action="">
            <!-- 
                Para lista múltiple se podrán seleccionar varias opciones
                En el name hay que usar un array para almacenar mas de una opción
            -->
            <select multiple name="ciudad[]" id="ciudad">
                <option value="SS">Santander</option>
                <option value="PA">Palencia</option>
                <option value="PO">Potes</option>
            </select>
            <button name="enviar">Enviar</button>
        </form>

    <?php
    } else {
        // Array con los posibles valores de la lista multiple
        $ciudades = [
            "SS" => "Santander",
            "PA" => "Palencia",
            "PO" => "Potes",
        ];

        //Guardamos  en la variable ciudad, el array con las ciudades seleccionadas
        $ciudad = $_GET["ciudad"];

        // Imprimimos el array con el value y las ciudades seleccionadas
        foreach ($ciudad as $indice => $valor) {
            echo "{$valor}-{$ciudades[$valor]}<br>";
        }
    }
    ?>
</body>

</html>