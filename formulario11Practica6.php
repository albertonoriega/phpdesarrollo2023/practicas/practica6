<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border: 1px solid black;
            border-collapse: collapse;
            width: 50%;
        }

        td {
            border: 1px solid black;
            padding: 10px;
        }

        td:nth-child(odd) {
            background-color: lightseagreen;
            text-align: end;
            width: 30%;
        }

        td:nth-child(even) {
            background-color: #ccc;
            width: 70%;
        }

        .filaboton {
            background-color: #ccc !important;
        }

        .filaboton button {
            margin-right: 50%;
            padding: 5px 15px;
        }
    </style>
</head>

<body>
    <?php

    //Inicializar las variables
    $nombre = "";
    $direccion = "";
    $correo = "";
    $contrasena1 = "";
    $contrasena2 = "";
    $dia = 0;
    $mes = "";
    $anio = 0;
    $sexo = "";
    // Los dos campos que se pueden seleccionar más de una opción, creamos un array con las posibles opciones
    $intereses = ["ficción", "terror", "acción", "comedia", "suspense"];
    $aficiones = ["Deportes al aire libre", "Deportes de aventuras", "Música pop", "Música rock", "Música alternativa", "Fotografía"];

    // Si se ha pulsado el botón
    if (isset($_GET["enviar"])) {
        $nombre = $_GET["nombre"];
        $direccion = $_GET["direccion"];
        $correo = $_GET["correo"];
        $contrasena1 = $_GET["contrasena1"];
        $contrasena2 = $_GET["contrasena2"];
        $dia = $_GET["dia"];
        $mes = $_GET["mes"];
        $anio = $_GET["anio"];
        $sexo = $_GET["sexo"];
        $interesesIntroducidos = $_GET["intereses"];
        $aficionesIntroducidos = $_GET["aficiones"];

        // Si las dos contraseñas son iguales imprimimos los valores seleccionados en el formulario
        if ($contrasena1 == $contrasena2) {
    ?>
            <div>Nombre completo: <?= $nombre ?> </div>
            <div>Direccion: <?= $direccion ?> </div>
            <div>Correo electrónico: <?= $correo ?> </div>
            <div>Fecha: <?= $dia . " " . $mes . " " . $anio ?> </div>
            <div>Sexo: <?= $sexo ?></div>
            <div>Intereses:
                <ul>
                    <?php
                    // Los intereses pueden ser mas de uno => imprimimos el array
                    foreach ($interesesIntroducidos as $valor) {
                        echo "<li>$intereses[$valor]</li>";
                    }
                    ?>
                </ul>
            </div>
            <div>Aficiones:
                <ul>
                    <?php
                    // Las aficiones pueden ser mas de una => imprimimos el array
                    foreach ($aficionesIntroducidos as $valor) {
                        echo "<li>$aficiones[$valor]</li>";
                    }
                    ?>
                </ul>
            </div>
        <?php
        } else {
            // Si las contraseñas no coinciden => caragamos de nuevo el formulario y enseñamos un mensaje
            cargarFormulario();
            echo "<div>Las contraseñas tienen que ser iguales</div>";
        }
    } else {
        // Si no se ha pulsado el botón => carga el formulario
        cargarFormulario();
        ?>

    <?php

    }

    /*
    Función que contiene el formulario
    */
    function cargarFormulario()
    {

    ?>
        <form action="formulario11Practica6.php">
            <table>
                <tr>
                    <td><label for="nombre">Nombre completo</label></td>
                    <td><input type="text" id="nombre" name="nombre" required></td>
                </tr>
                <tr>
                    <td><label for="direccion">Dirección</label></td>
                    <td><textarea name="direccion" id="direccion" cols="30" rows="10"></textarea></td>
                </tr>
                <tr>
                    <td><label for="correo">Correo electrónico</label></td>
                    <td><input type="email" id="correo" name="correo" required></td>
                </tr>
                <tr>
                    <td><label for="contrasena1">Contraseña</label></td>
                    <td><input type="password" id="contrasena1" name="contrasena1" required></td>
                </tr>
                <tr>
                    <td>
                        <label for="contrasena2"> Confirmar contraseña</label>
                        <br>
                        <br>
                        <label for="fecha">Fecha de nacimiento</label>
                    </td>
                    <td>
                        <input type="password" id="contrasena2" name="contrasena2" required>
                        <br>
                        <br>
                        <select name="mes" id="mes" required>
                            <option value="enero">enero</option>
                            <option value="febrero">febrero</option>
                            <option value="marzo">marzo</option>
                            <option value="abril">abril</option>
                            <option value="mayo">mayo</option>
                            <option value="junio">junio</option>
                            <option value="julio">julio</option>
                            <option value="agosto">agosto</option>
                            <option value="septiembre">septiembre</option>
                            <option value="octubre">octubre</option>
                            <option value="noviembre">noviembre</option>
                            <option value="diciembre">diciembre</option>

                        </select>
                        <select name="dia" id="dia" required>
                            <?php
                            for ($i = 1; $i < 32; $i++) {
                                echo "<option value=" . $i . ">$i</option>";
                            }
                            ?>

                        </select>
                        <input type="number" name="anio" id="anio" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <label for="sexo">Sexo</label>
                        <br>
                        <br>
                        <label for="intereses">Por favor elige los temas de tus intereses</label>
                    </td>
                    <td>
                        <input type="radio" name="sexo" id="hombre" value="hombre" required>Hombre
                        <input type="radio" name="sexo" id="mujer" value="mujer" required>Mujer
                        <br><br>
                        <!-- 
                            En los checkbox se pueden seleccionar mas de una opción
                            Para ello, en el name pasamos un array en cada checkbox
                            Ese array estará compuesto por los values de los checkbox seleccionados
                        -->
                        <input type="checkbox" name="intereses[]" id="0" value="0"> Ficción
                        <input type="checkbox" name="intereses[]" id="1" value="1">Terror
                        <br>
                        <input type="checkbox" name="intereses[]" id="2" value="2">Acción
                        <input type="checkbox" name="intereses[]" id="3" value="3"> Comedia
                        <br>
                        <input type="checkbox" name="intereses[]" id="4" value="4"> Suspense
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="aficiones">Selecciona tus aficiones <br> (Selecciona múltiples elementos pulsando la
                            tecla control y haciendo clic en cada uno, uno a uno)</label>
                    </td>
                    <!-- 
                            En las listas multiples se pueden seleccionar mas de una opción
                            Para ello, en el name del select pasamos un array
                            Ese array estará compuesto por los values de los option seleccionados
                        -->
                    <td><select multiple name="aficiones[]" id="aficiones[]">
                            <option value="0">Deportes al aire libre</option>
                            <option value="1">Deportes de aventuras</option>
                            <option value="2">Música Pop</option>
                            <option value="3">Música Rock</option>
                            <option value="4">Música alternativa</option>
                            <option value="5">Fotografía</option>
                        </select></td>
                </tr>
                <tr>
                    <td class="filaboton" colspan="2"> <button name="enviar">Enviar</button></td>

                </tr>
            </table>
        </form>
    <?php
    }
    ?>

</body>

</html>