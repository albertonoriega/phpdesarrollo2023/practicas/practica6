<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    if ($mal) {

    ?>
        <form action="">
            <legend>Selecione una ciudad</legend>

            <!-- Con los radio solo se podrá seleccionar una opción-->
            <input type="radio" name="ciudad" id="SS" value="SS">
            <label for="Ss">Santander</label>
            <input type="radio" name="ciudad" id="PO" value="PO">
            <label for="Ss">Potes</label>
            <input type="radio" name="ciudad" id="PA" value="PA">
            <label for="Ss">Palencia</label>
            <button name="enviar">Enviar</button>
        </form>

    <?php
    } else {
        // Array con los posibles valores de los radio
        $ciudades = [
            "SS" => "Santander",
            "PA" => "Palencia",
            "PO" => "Potes",
        ];

        $ciudad = $_GET['ciudad'];

        // Imprimimos el array con el value y las ciudades seleccionadas
        echo "El elemento seleccionado es: {$ciudades[$ciudad]} ";
    }
    ?>
</body>

</html>