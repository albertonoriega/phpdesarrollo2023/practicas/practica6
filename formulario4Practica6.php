<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    if ($mal) {

    ?>
        <form action="">
            <select name="ciudad" id="ciudad">
                <option value="SS">Santander</option>
                <option value="PA">Palencia</option>
                <option value="PO">Potes</option>
            </select>
            <button name="enviar">Enviar</button>
        </form>

    <?php
    } else {
        $ciudades = [
            "SS" => "Santander",
            "PA" => "Palencia",
            "PO" => "Potes",
        ];
        $ciudad = $_GET["ciudad"];
        echo $ciudades[$ciudad];
    }
    ?>
</body>

</html>