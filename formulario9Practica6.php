<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form action="">
        <label for="ciudad">Seleccione ciudad</label>
        <select name="ciudad">
            <option value="0"></option>
            <optgroup label="Asia">
                <option value="3">Delhi</option>
                <option value="4">Hong Kong</option>
                <option value="8">Mumbia</option>
                <option value="11">Tokio</option>
            </optgroup>
            <optgroup label="Europe">
                <option value="1">Amsterdam</option>
                <option value="5">London</option>
                <option value="7">Moscu</option>
            </optgroup>
            <optgroup label="North America">
                <option value="6">Los Angeles</option>
                <option value="9">New York</option>
            </optgroup>
            <optgroup label="South America">
                <option value="2">Buenos Aires</option>
                <option value="10">Sao Paulo</option>
            </optgroup>
        </select>
        <button name="enviar">Enviar</button>

    </form>

    <?php
    // Array de las ciudades
    $ciudades = ["", "Amsterdam", "Buenos Aires", "Delhi", "Hong Kong", "London", "Los Angeles", "Moscu", "Mumbai", "New York", "Sao Paulo", "Tokio"];

    if (isset($_GET["enviar"])) {
        // Guardamos el value de la ciudad seleccionada
        $ciudad = $_GET['ciudad'];

        //Imprimimos la ciudad
        echo $ciudades[$ciudad];
    }
    ?>
</body>

</html>