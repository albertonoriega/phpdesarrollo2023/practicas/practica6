<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    if ($mal) {

    ?>
        <form action="">
            <legend>Selecione una ciudad</legend>
            <!-- 
                Con los checkbox se podrán seleccionar varias opciones
                En el name de cada checkbox hay que usar un array para almacenar mas de una opción
            -->
            <input type="checkbox" name="ciudad[]" id="SS" value="SS">
            <label for="Ss">Santander</label>
            <input type="checkbox" name="ciudad[]" id="PO" value="PO">
            <label for="Ss">Potes</label>
            <input type="checkbox" name="ciudad[]" id="PA" value="PA">
            <label for="Ss">Palencia</label>
            <button name="enviar">Enviar</button>
        </form>

    <?php
    } else {
        // Array con los posibles valores de los checkbox
        $ciudades = [
            "SS" => "Santander",
            "PA" => "Palencia",
            "PO" => "Potes",
        ];

        // Imprimimos el array con el value y las ciudades seleccionadas
        echo "Los elementos seleccionados son: ";
        foreach ($_GET['ciudad'] as $value) {
            echo "<br>$value-$ciudades[$value]";
        }
    }
    ?>
</body>

</html>